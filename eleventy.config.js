const { EleventyHtmlBasePlugin } = require('@11ty/eleventy')
const fs = require('fs')
const postcss = require('postcss')
const postcssimport = require('postcss-import')
const glob = require('glob')
const path = require('path')
const fse = require('fs-extra')

module.exports = function (eleventyConfig) {
  eleventyConfig.addPlugin(EleventyHtmlBasePlugin)

  // eleventyConfig.addFilter("searchSingle", searchFilterSingle);
  eleventyConfig.addCollection('articles', (collection) => {
    return [...collection.getFilteredByGlob('./src/content/articles/**.html')]
  })

  eleventyConfig.addPassthroughCopy({ 'static/css': '/css' })
  eleventyConfig.addPassthroughCopy({ 'static/themes': '/css/themes' })
  eleventyConfig.addPassthroughCopy({ 'static/js': '/js' })
  eleventyConfig.addPassthroughCopy({ 'static/outputs': '/outputs' })
  eleventyConfig.addPassthroughCopy({ 'static/images': '/images' })

  eleventyConfig.on('eleventy.after', async () => {
    // Run me after the build endsconst fse = require('fs-extra');

    const themes = await glob('./static/themes/**/print.css')
    console.log(themes)

    themes.forEach((theme) => {
      // get the theme nameg
      let themeName = theme.split('/')[2]
      // check if an output folder exist or create it
      if (!fs.existsSync(path.join(__dirname, `stylesheets/${themeName}`))) {
        fs.mkdir(path.join(__dirname, `stylesheets/${themeName}`), (err) => {
          if (err) {
            return console.log(err)
          }
        })
      }
      console.log(`Create a folder ${themeName}`)

      // post css, create the css file!
      postcss()
        .use(postcssimport)
        .process(fs.readFileSync(theme, 'utf8'), {
          from: theme,
        })
        .then((result) => {
          let output = result.css.replace(/\.\.\/fonts/, 'fonts')
          fs.writeFileSync(`stylesheets/${themeName}/${themeName}.css`, output)
        })

      //copy the fonts
      try {
        fse.copySync(
          `./static/themes/${themeName}/fonts`,
          `stylesheets/${themeName}/fonts/`,
          { overwrite: true }
        )
      } catch (err) {
        console.error(err)
      }
    })
  })

  // post css for each templates when finished

  // plugin TOC

  // folder structures
  // -----------------------------------------------------------------------------
  // content, data and layouts comes from the src folders
  // output goes to public (for gitlab ci/cd)
  // -----------------------------------------------------------------------------
  return {
    dir: {
      input: 'src',
      output: 'public',
      includes: 'layouts',
      data: 'data',
    },
  }
}
